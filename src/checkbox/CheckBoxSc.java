package checkbox;

/**
 *
 * @author Josue Daniel Roldan Ochoa
 */

import java.awt.CheckboxGroup;
import javax.swing.JFrame;
import javax.swing.JCheckBox;
import javax.swing.JPanel;

public class CheckBoxSc {
    
public void CheckBox(){
JFrame fm= new JFrame();
fm.setResizable(false);
fm.setBounds(0,0,200,150);
fm.setLocationRelativeTo(null);
fm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
CheckboxGroup cbg= new CheckboxGroup();
JCheckBox cx= new JCheckBox("Hombre",false);
cx.setBounds(0,0,200,100);
JCheckBox c= new JCheckBox("Mujer",false);
c.setBounds(0,0,200,100);
JPanel pl= new JPanel();
pl.setBounds(0,0,200,150);
pl.add(c);
pl.add(cx);
fm.add(pl);
fm.setVisible(true);
}
}
